#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>
#include <string>

using namespace std;
using namespace AmqpClient;

const string ex_inp = "x_num";
const string q_input = "q_numbers";
const string ex_res = "x_res";
const string q_res = "q_res";

const string addr = "test.czterybity.pl";
const int port = 5672;
const string user = "admin";
const string pass = "tymczasowe";

bool is_prime(long n)
{
    for (long div = 2 ; div < n ; ++div)
        if( n % div == 0) return false;
    return true;
}

void check()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);
    channel->BasicConsume(q_input,"", true, false, false, 1);
    for(;;)
    {
        auto env = channel->BasicConsumeMessage();
        long n = stol(env->Message()->Body());
        cout << "got " << n << endl;
        if(is_prime(n))
        {
            cout << n << " is prime " << endl;
            channel->BasicPublish(ex_res, "",
                                  BasicMessage::Create(string("Leszek ") + to_string(n)));


        }
        channel->BasicAck(env);
        cout << "Ack" << endl;
    }
}



int main()
{
    check();
}

