#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello render server" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_ROUTER);
    socket.bind("tcp://*:8888");

    for(int i = 0 ; i < 768; ++i)
    {
        string addres = s_recv(socket);
        s_recv(socket);
        string client_name = s_recv(socket);
        cout << "got request from: " << addres << " : " << client_name << endl;
        s_sendmore(socket, addres);
        s_sendmore(socket, "");
        s_send(socket, to_string(i) + " 4");
        this_thread::sleep_for(chrono::milliseconds(200));
    }

    return 0;
}

