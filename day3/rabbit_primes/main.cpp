#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>

using namespace std;
using namespace AmqpClient;

const string ex_inp = "x_num";
const string q_input = "q_numbers";
const string ex_res = "x_res";
const string q_res = "q_res";

const string addr = "test.czterybity.pl";
const int port = 5672;
const string user = "admin";
const string pass = "tymczasowe";

bool is_prime(long n)
{
    for (long div = 2 ; div < n ; ++div)
        if( n % div == 0) return false;
    return true;
}

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);
    channel->DeclareExchange(ex_inp, Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareExchange(ex_res, Channel::EXCHANGE_TYPE_DIRECT);

    channel->DeclareQueue(q_input, false, false, false, false);
    channel->DeclareQueue(q_res, false, false, false, false);

    channel->BindQueue(q_input, ex_inp,  "");
    channel->BindQueue(q_res, ex_res,  "");

    for(long i = 10'000'000 ; i < 10'100'000 ; ++i)
    {
        channel->BasicPublish(ex_inp, "" , BasicMessage::Create(to_string(i)));
    }
}


void print()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);
    channel->BasicConsume(q_res, "", true, true, false);
    for(;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    thread th1(send);
    thread th2(print);
    th1.join();
    th2.join();

}

