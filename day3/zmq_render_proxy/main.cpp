#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello proxy!" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket_in(context, ZMQ_PULL);
    zmq::socket_t socket_out(context, ZMQ_PUSH);
    socket_in.bind("tcp://*:7777");
    socket_out.bind("tcp://*:9999");
    for(;;)
    {
        s_send(socket_out, s_recv(socket_in));
    }
    return 0;
}

