#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>
#include "smallpt_lib.hpp"

using namespace std;

// scheme:
// Leszek: 123 :: rgb rgb

int main()
{
    cout << "Hello zmq render worker" << endl;
    zmq::context_t context(1);
    zmq::socket_t soc_in(context, ZMQ_REQ);
    zmq::socket_t soc_out(context, ZMQ_PUSH);

    srand(time(NULL));
    s_set_id(soc_in);

    soc_in.connect("tcp://test.czterybity.pl:8888");
    soc_out.connect("tcp://test.czterybity.pl:7777");
    for(;;)
    {
        s_send(soc_in, "Leszek");
        auto line_str = s_recv(soc_in);
        cout << "got: " << line_str;
        istringstream iss(line_str);
        int line_no;
        int quality;
        iss >> line_no >> quality;
        auto line = scan_line(line_no, quality);
        string output;
        output = string("Leszek: ") + to_string(line_no)
                + " :: " + line;

        s_send(soc_out, output);
        cout << "... finish" << endl;
    }
    return 0;
}
