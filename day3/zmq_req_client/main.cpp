#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello REQ-REP client" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ);
    socket.connect("tcp://test.czterybity.pl:6666");

    for(;;)
    {
        this_thread::sleep_for(chrono::seconds(1));
        s_send(socket, "Leszek asking");
        cout << "got answer " << s_recv(socket) << endl;
    }
    return 0;
}

