#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello SUB client" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_SUB);
    socket.connect("tcp://test.czterybity.pl:6667");
    socket.setsockopt(ZMQ_SUBSCRIBE, 0, 0);

    for(;;)
    {
        cout << "got answer " << s_recv(socket) << endl;
    }
    return 0;
}

