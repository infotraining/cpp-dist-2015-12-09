#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>

using namespace std;
using namespace AmqpClient;

const string addr = "test.czterybity.pl";
const int port = 5672;
const string user = "admin";
const string pass = "tymczasowe";
const string ex_name = "chat_luxoft";

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);
    for (;;)
    {
        string msg;
        cout << ">>> " << endl;
        getline(cin, msg);
        channel->BasicPublish(ex_name, "", BasicMessage::Create("[Leszek] " + msg));
    }
}

void receive()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);
    string q_name = channel->DeclareQueue("");
    channel->BindQueue(q_name, ex_name, "");
    channel->BasicConsume(q_name, "", true, true, false);
    for(;;)
    {
        cout << channel->BasicConsumeMessage()->Message()->Body() << endl;
    }
}

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);
    channel->DeclareExchange(ex_name, Channel::EXCHANGE_TYPE_FANOUT);
}

int main()
{
    setup();
    thread th1(send);
    thread th2(receive);
    th1.join();
    th2.join();
    return 0;
}

