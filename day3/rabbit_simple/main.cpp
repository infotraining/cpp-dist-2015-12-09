#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <thread>

using namespace std;
using namespace AmqpClient;

const string addr = "test.czterybity.pl";
const int port = 5672;
const string user = "admin";
const string pass = "tymczasowe";
const string ex_name = "luxoft";
const string q_name = "qluxoft2";

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);

    channel->DeclareExchange(ex_name, Channel::EXCHANGE_TYPE_DIRECT);
    channel->DeclareQueue(q_name, false, false, true, false);
    channel->BindQueue(q_name, ex_name , "");

//    for (int i = 0 ; i < 1500 ; ++i)
//    {
//        channel->BasicPublish(ex_name, "", BasicMessage::Create("hello " + to_string(i)));
//        this_thread::sleep_for(100ms);
//        cout << "." << flush;
//    }
}

void receive()
{
    Channel::ptr_t channel;
    channel = Channel::Create(addr, port, user, pass);
    channel->BasicConsume(q_name, "", true, true, true);
    for(;;)
    {
        cout << channel->BasicConsumeMessage()->Message()->Body() << endl;
    }
}

int main()
{
    send();
    receive();
    return 0;
}

