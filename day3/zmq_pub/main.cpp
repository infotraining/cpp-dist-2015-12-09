#include <iostream>
#include <zmq.hpp>
#include <zhelpers.hpp>
#include <thread>

using namespace std;

int main()
{
    cout << "Hello PUB server" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_PUB);
    socket.bind("tcp://*:6666");

    for(long i = 0 ; i < 100000000; ++i)
    {
        this_thread::sleep_for(chrono::seconds(1));
        s_send(socket, "msg from server " + to_string(i));
    }
    return 0;
}

