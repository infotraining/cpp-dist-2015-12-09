#include <iostream>
#include <vector>
#include <thread>
#include <mutex>

using namespace std;
long counter = 0;
timed_mutex mtx;

void worker(int id)
{
    cout << "Worker has started " << this_thread::get_id() << endl;
    unique_lock<timed_mutex> lg(mtx, try_to_lock);
    if (!lg.owns_lock())
    {
        cout << "i don't have lock " << this_thread::get_id() << endl;
        while(!lg.try_lock_for(500ms))
        {
            cout << "waiting..." << endl;
        }
    }
    cout << "i have lock " << this_thread::get_id() << endl;
    counter++;
    this_thread::sleep_for(2s);
    cout << "releasing lock " << this_thread::get_id() << endl;
}

int main()
{
    vector<thread> thds;
    thds.emplace_back(worker,1);
    thds.emplace_back(worker,2);
    //thds.emplace_back(worker,3);
    for(auto& th : thds) th.join();
    return 0;
}

