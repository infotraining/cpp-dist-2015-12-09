#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex q_mtx;
condition_variable cond;

void producer()
{
    for(int i = 0 ; i < 100 ; ++i)
    {
        {
            lock_guard<mutex> lg(q_mtx);
            q.push(i);
            cond.notify_one();
        }
        this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> lg(q_mtx);
//        while(q.empty())
//        {
//            cond.wait(lg);
//        }

        cond.wait(lg, [&] {return !q.empty();});
        int msg = q.front();
        q.pop();
        lg.unlock();
        cout << id << " got " << msg << endl;

    }
}

int main()
{
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    for(auto& th : thds) th.join();
    return 0;
}

