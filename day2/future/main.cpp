#include <iostream>
#include <future>
#include <thread>

using namespace std;

int question()
{
    this_thread::sleep_for(1s);
    return 42;
}

int main()
{
    future<int> res = async(question);
    cout << "Data is processed" << endl;
    cout << "Result = " << res.get() << endl;

    packaged_task<int(void)> pt(question);
    auto pt_res = pt.get_future();
    thread th(move(pt));
    th.detach();
    cout << "Result = " << pt_res.get() << endl;
    return 0;
}

