#include <iostream>
#include <thread>
#include <vector>
#include <queue>
#include <mutex>
#include <condition_variable>
#include "thread_safe_queue.h"

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for(int i = 0 ; i < 100 ; ++i)
    {
        q.push(i);
        //this_thread::sleep_for(100ms);
    }
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg);
        this_thread::sleep_for(100ms);
        // optional if (q.pop_nowait())
        cout << id << " got " << msg << endl;
    }
}

int main()
{
    vector<thread> thds;
    cout << "number of cores " << thread::hardware_concurrency() << endl;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    for(auto& th : thds) th.join();
    return 0;
}

