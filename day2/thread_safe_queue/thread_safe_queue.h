#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H
#include <mutex>
#include <condition_variable>
#include <queue>

template<typename T>
class thread_safe_queue
{
    std::queue<T> q;
    std::condition_variable cond;
    std::mutex m;
public:    
    void push(const T& item)
    {
        std::lock_guard<std::mutex> lg(m);
        q.push(item);
        cond.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lg(m);
        cond.wait(lg, [&] {return !q.empty();});
        item = q.front();
        q.pop();
    }

    bool pop_nowait(T& item)
    {
        std::lock_guard<std::mutex> lg(m);
        if (q.empty()) return false;
        item = q.front();
        q.pop();
        return true;
    }

};

#endif // THREAD_SAFE_QUEUE_H
