#include <iostream>
#include <vector>
#include <thread>
#include <functional>
#include "../thread_safe_queue/thread_safe_queue.h"

using namespace std;

using task_t = std::function<void()>;

class thread_pool
{
    thread_safe_queue<task_t> q;
    vector<thread> workers;

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if (!task) return;
            task();
        }
    }

public:
    thread_pool(int pool_size)
    {
        for(int i = 0 ; i < pool_size ; ++i)
            workers.emplace_back(&thread_pool::worker, this);
    }

    void submit(task_t t)
    {
        if (t) q.push(t);
    }

    ~thread_pool()
    {
        for(int i = 0 ; i < workers.size() ; ++i) q.push(task_t());
        for(auto& th : workers) th.join();
    }
};

void f()
{
    cout << "f() called" << endl;
}

int main()
{
    thread_pool tp(4);
    tp.submit(f);
    tp.submit([]{ cout << "lambda" << endl;});
    for (int i = 0 ; i < 100 ; ++i)
    {
        tp.submit([i] { cout << "lambda " << i << endl;
                        this_thread::sleep_for(1s);});
    }
    return 0;
}

