#include <random>
#include <iostream>
#include <chrono>
#include <vector>
#include <thread>

using namespace std;

const double true_pi = 3.141592653589793;

void check_pi(long& counter, const long& N)
{
    std::random_device rd;
    std::mt19937_64 gen(rd());
    long c{};
    std::uniform_real_distribution<> dis(0, 1);
    for (int n = 0; n < N; ++n) {
        double x = dis(gen);
        double y = dis(gen);
        if (x*x + y*y < 1)
            c++;
    }
    counter = c;
}

int main()
{
    long count{};

    long N = 100000000;

    vector<int> n_of_thds{1,2,4,8,16,32};
    for (int n : n_of_thds)
    {
        auto start = chrono::steady_clock::now();
        int thread_count = n;
        vector<thread> thds;
        vector<long> counts(thread_count);
        //long counts;

        for(int i = 0 ; i < thread_count ; ++i)
        {
            thds.emplace_back(check_pi, ref(counts[i]), N/thread_count);
        }

        for(auto& th: thds) th.join();
        double pi = 4*(double)accumulate(counts.begin(), counts.end(), 0)/N;
        //double pi = 4*(double)counts/N;

        auto end = chrono::steady_clock::now();

        cout << "thread count = " << n << endl;
        cout << "     Pi = " << pi << endl;
        cout << "true Pi = " << true_pi << endl;
        cout << "elapsed = " << chrono::duration_cast<chrono::milliseconds>(end-start).count() << endl;
    }
}
