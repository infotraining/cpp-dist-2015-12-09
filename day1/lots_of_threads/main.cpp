#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
    this_thread::sleep_for(200ms);
}

void params(int a)
{
    cout << "got " << a << endl;
}

void modify(int& val)
{
    val++;
}

thread generate()
{
    thread th(hello);
    return th;
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> threads;
    threads.push_back(thread(hello));
    threads.emplace_back(params, 4);

    threads.push_back(generate());
    threads.emplace_back(generate());

    for (thread& th : threads) th.join();

    return 0;
}

