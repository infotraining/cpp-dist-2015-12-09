#include <iostream>
#include <thread>

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
    this_thread::sleep_for(200ms);
}

void params(int a)
{
    cout << "got " << a << endl;
}

void modify(int& val)
{
    val++;
}

thread generate()
{
    thread th(hello);
    return th;
}

int main()
{
    cout << "Hello World!" << endl;
    thread th1(hello);
    cout << "after launching thread" << endl;
    thread th2(params, 2);
    int a = 3;
    thread th3(modify, std::ref(a));
    thread th4(generate());
    thread th5;


    th1.join();
    th2.join();
    th3.join();
    //th4.join();
    th4.detach();
    if (th5.joinable())
        th5.join();
    cout << "a = " << a << endl;
    return 0;
}

