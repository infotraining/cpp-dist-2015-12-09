#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>

using namespace std;

long counter = 0;
long m_counter = 0;
atomic<long> at_counter{};
mutex mtx;

class LockGuard
{
    mutex& m_;
public:
    LockGuard(mutex& mtx) : m_(mtx)
    {
        m_.lock();
    }

    ~LockGuard()
    {
        m_.unlock();
    }

    LockGuard(const LockGuard&) = delete;
    LockGuard& operator=(const LockGuard&) = delete;
};

void inc()
{
    for (int i = 0 ; i < 10000 ; ++i)
        counter++;
}

void inc_mtx()
{
    for (int i = 0 ; i < 10000 ; ++i)
    {
        /*mtx.lock();
        m_counter++;
        if (m_counter == 100) return;
        mtx.unlock();*/
        LockGuard l(mtx);
        //lock_guard<mutex> l(mtx);
        //if (m_counter == 100) return;
        m_counter++;
    }
}

void atomic_inc()
{
    for (int i = 0 ; i < 10000 ; ++i)
        at_counter.fetch_add(1, memory_order_seq_cst);
}

int main()
{
    //inc();
    {
        auto start = chrono::steady_clock::now();
        vector<thread> thds;
        for (int i = 0 ; i < 4 ; ++i)
        {
            thds.emplace_back(inc);
        }

        for(auto& th : thds) th.join();
        auto end = chrono::steady_clock::now();
        cout << "Counter = " << counter << endl;
        cout << "elapsed = " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;
    }

    {
        auto start = chrono::steady_clock::now();
        vector<thread> thds;
        for (int i = 0 ; i < 4 ; ++i)
        {
            thds.emplace_back(inc_mtx);
        }

        for(auto& th : thds) th.join();
        auto end = chrono::steady_clock::now();
        cout << "Counter = " << m_counter << endl;
        cout << "elapsed = " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;
    }

    {
        auto start = chrono::steady_clock::now();
        vector<thread> thds;
        cout << "is lock free " << at_counter.is_lock_free() << endl;
        for (int i = 0 ; i < 4 ; ++i)
        {
            thds.emplace_back(atomic_inc);
        }

        for(auto& th : thds) th.join();
        auto end = chrono::steady_clock::now();
        cout << "Counter = " << at_counter << endl;
        cout << "elapsed = " << chrono::duration_cast<chrono::microseconds>(end-start).count() << endl;
    }

}

