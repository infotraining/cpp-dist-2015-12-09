## Multithreading and distributed programming in C++

### Additonal information

#### login and password for VM:

```
dev  /  tymczasowe
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### GIT

```
git clone https://bitbucket.org/infotraining/cpp-dist-2015-12-09
```

[git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

### Books

[Linux Programming](http://shop.oreilly.com/product/0636920026891.do)


### Linki

http://blog.codinghorror.com/the-infinite-space-between-words/

http://bronikowski.com/2504/niesamowita-laska

http://www.1024cores.net/

http://preshing.com/

http://herbsutter.com/

http://www.cs.wustl.edu/~schmidt/ACE.html

https://software.intel.com/en-us/intel-tbb

http://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-an-unsorted-array

http://blog.bfitz.us/?p=491 - fibers, cooperative scheduling

http://lwn.net/Articles/250967/ - What every programmer should know about memory

